
export const doGet = (e) => {

  //const {a,b,c} = e.parameter; // will be {"a": "1", "b": "2", "c": "3"}. For parameters that have multiple values, this only returns the first value


  let template = HtmlService.createTemplateFromFile('ui')
  //pass variables to template
  let greeting = {text:"This is a property send from doGet()"}
  template.reactProp = escape(JSON.stringify(greeting))

  let html = template.evaluate()


  return html
}
export const doPost = (e)=>{
  let output = doGet(e)
  //output.setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL);
  return output
}

export const helloWorld = (name)=>{
  return `Hello ${name}. This was received from the Google Service.`
}
