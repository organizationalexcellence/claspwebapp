import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import server from '../../utils/server';
const { serverFunctions } = server;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


export default function App(props) {
  const classes = useStyles();
  const [isSending,setIsSending] = React.useState(false);
  const [sendComplete,setSendComplete] = React.useState(false);
  const [hello,setHello] = React.useState("")
  const textFieldRef = React.useRef();
  function send(){
    setIsSending(true)

    serverFunctions.helloWorld(textFieldRef.current.value).then((data)=>{
      setSendComplete(true)
      setIsSending(false)
      console.log("Send Complete")
      setHello(data)
    })

  }
  const interactiveSection=()=>{
    if (isSending){
      return (<CircularProgress />)
    }else{
      if (sendComplete){
        return (<Typography>{hello}</Typography>)
      }else{
        return (
          <div>
            <div>
              <TextField id="outlined-basic" label="Enter your name" variant="outlined" inputRef={textFieldRef}/>
              <Button variant="contained" color="primary" onClick={send}>Send Greeting</Button>
            </div>
          </div>
        )
      }

    }
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h1">{props.reactProp.text}</Typography>
        </Grid>
        <Grid item xs={12}>
          {interactiveSection()}
        </Grid>
      </Grid>
    </div>
  );
}
